﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.InMission.UI
{
    public class MissionStatusView : MonoBehaviour
    {
        [SerializeField] Text roundStart;
        [SerializeField] GameObject roundWin;
        [SerializeField] GameObject gameOver;
        [SerializeField] GameObject gameWin;
        [SerializeField] CanvasGroup canvasGroup;
        [SerializeField] float transitionDuration;
        [SerializeField] float fadeOutSmoothness;

        public void ShowStartRound(string roundName)
        {
            canvasGroup.alpha = 1;
            roundStart.text = $"ROUND {roundName}";
            roundStart.gameObject.SetActive(true);
            Invoke("StartFadeOut", transitionDuration);
        }

        public void ShowRoundWin()
        {
            canvasGroup.alpha = 1;
            roundWin.SetActive(true);
        }

        public void ShowGameOver()
        {
            canvasGroup.alpha = 1;
            gameOver.SetActive(true);
        }

        void StartFadeOut() => 
            StartCoroutine(FadeOut());

        IEnumerator FadeOut()
        {
            while (canvasGroup.alpha > 0)
            {
                canvasGroup.alpha -= fadeOutSmoothness;
                yield return null;
            }
        }

        public void ShowGameWin()
        {
            canvasGroup.alpha = 1;
            gameWin.SetActive(true);
        }
    }
}
