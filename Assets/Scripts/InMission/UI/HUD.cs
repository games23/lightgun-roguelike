﻿using Assets.Scripts.GameCommon;
using UnityEngine;

namespace Assets.Scripts.InMission.UI
{
    public class HUD : MonoBehaviour
    {
        HurtIndicator hurtIndicator;
        Animator fade;
        [SerializeField] WaveCounter waves;
        [SerializeField] CashCounter cash;
        
        public void ShowGameOver() => Debug.Log("YOU ARE DEAD");

        public void ShowFadeOut() => fade.SetTrigger("FadeOut");

        public void UpdateWaves(int currentWave, int maxWaves) => waves.UpdateWaves(currentWave, maxWaves);
        public void UpdateCash(int amount) => cash.UpdateCash(amount);
    }
}
