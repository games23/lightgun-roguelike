﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.InMission.UI
{
    public class TunnelVision : MonoBehaviour
    {
        public Image Tunnel;
        private float aimSpeed;
        private float targetStrangth;
        private float currentStrength;

        public void AimEffect(float targetTunnel, float speed)
        {
            targetStrangth = targetTunnel;
            aimSpeed = speed;
        }

        public void ResetView(float speed)
        {
            targetStrangth = 0;
            aimSpeed = speed;
        }

        void Update()
        {
            if (aimSpeed > 0)
            {
                currentStrength = Mathf.Lerp(currentStrength, targetStrangth, aimSpeed);
                SetAlpha(Tunnel, currentStrength);
            }
        }

        private void SetAlpha(Image tunnel, float alpha)
        {
            var color = tunnel.color;
            color.a = alpha;
            tunnel.color = color;
        }
    }
}
