﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.InMission.UI
{
    public class DynamicLabel : MonoBehaviour
    {
        public string Format;

        private Text label;
        private Text Label
        {
            get
            {
                if (!label)
                    label = GetComponent<Text>();

                return label;
            }
        }

        public void UpdateText(params object[] arguments) => Label.text = string.Format(Format, arguments);
    }
}
