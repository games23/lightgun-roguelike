﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.InMission.UI
{
    public class WaveCounter : MonoBehaviour
    {
        [SerializeField] Text counterText;
        [SerializeField] string counterFormat;
        [SerializeField] Animation animation;

        public void UpdateWaves(int waveNumber, int maxWaves)
        {
            counterText.text = string.Format(counterFormat, waveNumber, maxWaves);
            animation.Play();
        }
    }
}
