﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.InMission.UI
{
    public class UITracking : MonoBehaviour
    {
        [SerializeField] Transform target;

        void Update()
        {
            if(target)
                transform.position = Camera.main.WorldToScreenPoint(target.position);
        }
    }
}
