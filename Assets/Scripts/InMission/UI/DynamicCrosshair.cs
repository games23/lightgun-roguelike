﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.InMission.UI
{
    public class DynamicCrosshair : MonoBehaviour
    {
        [SerializeField] RectTransform outerBound;
        [SerializeField] float minSize;
        [SerializeField] float maxSize;

        void Start()
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = false;
        }

        void Update()
        {
            transform.position = Input.mousePosition;
        }

        public void ScaleToAccuracy(float inaccuracy)
        {
            var accuracyToSize = Mathf.Lerp(minSize, maxSize, inaccuracy);
            outerBound.sizeDelta = new Vector2(accuracyToSize, accuracyToSize);
        }
    }
}
