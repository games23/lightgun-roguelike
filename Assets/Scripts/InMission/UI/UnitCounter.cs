﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.InMission.UI
{
    public class UnitCounter : MonoBehaviour
    {
        [SerializeField] GameObject unitPrefab;
        [SerializeField] HorizontalLayoutGroup layout;
        [SerializeField] int maxCapacity;

        int count;
        int maxCount;
        List<GameObject> units;

        public void Initialize(int targetCapacity)
        {
            RemoveExistingUnits();
            count = targetCapacity;
            maxCount = targetCapacity;
            units = new List<GameObject>();

            var unitCount = Mathf.Max(targetCapacity, maxCapacity);
            foreach (var _ in Enumerable.Range(0, targetCapacity))
            {
                var unit = Instantiate(unitPrefab, transform);
                units.Add(unit);
            }
        }

        void RemoveExistingUnits()
        {
            if (units != null)
            {
                foreach(var u in units)
                    Destroy(u);
            }
        }

        public void Spend()
        {
            count--;
            if (count <= maxCapacity)
                units.Last(u => u.activeInHierarchy).SetActive(false);
        }

        public void Reset()
        {
            count = maxCount;
            units.ForEach(bu => bu.SetActive(true));
        }
    }
}
