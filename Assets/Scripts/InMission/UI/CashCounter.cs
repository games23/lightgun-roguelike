﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.InMission.UI
{
    public class CashCounter : MonoBehaviour
    {
        [SerializeField] Text counterText;
        [SerializeField] string counterFormat;
        [SerializeField] Animation animation;

        public void UpdateCash(int amount)
        {
            counterText.text = string.Format(counterFormat, amount);
            animation.Play();
        }
    }
}
