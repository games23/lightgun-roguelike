﻿using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Scenery;
using UnityEngine;

namespace Assets.Scripts.InMission.Weapons
{
    public class HitscanWeapon : Weapon
    {
        public LayerMask HittableLayerMask;
     
        public override void Fire()
        {
            var deviationOffset = Random.insideUnitSphere * Inaccuracy;
            var direction = ((transform.forward * 20f) + deviationOffset).normalized; 
            
            CastFireRay(FirePosition, direction);
            View.ShowFire(direction);
            currentAmmo--;
            
            CycleRound();

            currentAccuracy = Mathf.Clamp(currentAccuracy - BaseStats.RecoilStrength, 1f, BaseStats.Accuracy);
            onAccuracyChanged(Inaccuracy);

            //Debug.Log($"acc: {currentAccuracy} / inacc: {Inaccuracy}");
        }

        protected void CastFireRay(Vector3 firePosition, Vector3 direction)
        {
            var ray = new Ray(FirePosition, direction);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100f, HittableLayerMask))
            {
                CheckSurfaceHit(hit);
                CheckDamageableHit(hit);
                CheckPush(hit);
            }
        }

        private void CheckPush(RaycastHit hit)
        {
            var pushable = hit.collider.GetComponent<Push>();
            pushable?.PushTo(transform.forward, CurrentStats.Damage * 100);
        }
        
        private void CheckDamageableHit(RaycastHit hit)
        {
            var armorPiece = hit.collider.GetComponent<Damageable>();
            armorPiece?.GetHit(CurrentStats.Damage);
        }

        void CheckSurfaceHit(RaycastHit hit) => 
            hit.collider.GetComponent<HitSurface>()?.HitHere(hit.point, hit.normal);
    }
}
