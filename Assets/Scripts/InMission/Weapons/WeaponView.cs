﻿using Assets.Scripts.InMission.Common;
using UnityEngine;

namespace Assets.Scripts.InMission.Weapons
{
    public class WeaponView : MonoBehaviour
    {
        public Animator Animator;
        float SlowestFiringFactor = 0.4f;
        private WeaponStats stats;
        
        [Header("sounds")]
        public RandomPitchSfx FireSfx;
        public AudioSource ReloadSfx;
        public AudioSource FinishReloadSfx;

        [Header("particles")]
        public ParticleSystem MuzzleVfx;
        public ParticleSystem TracerVfx;
        public Vector3 MuzzlePosition => muzzleTransform.position;
        public Transform muzzleTransform => MuzzleVfx.transform;
        public Transform tracerTransform => TracerVfx.transform;

        public void ShowFire(Vector3 direction)
        {
            UpdateSpeedFactors();
            tracerTransform.LookAt(tracerTransform.position + direction);
            Animator.SetTrigger("Fire");
            FireSfx.Play();
            MuzzleVfx.Play();
            TracerVfx.Play();
        }

        public void ShowFire() => ShowFire(transform.forward);

        public void Initialize(WeaponStats stats)
        {
            this.stats = stats;
            UpdateSpeedFactors();
        }

        void UpdateSpeedFactors()
        {
            var cycleFactor = stats.CycleRate > SlowestFiringFactor ? SlowestFiringFactor : stats.CycleRate;
            Animator.SetFloat("FireSpeedFactor", 1f / cycleFactor);
            Animator.SetFloat("ReloadSpeedFactor", 1f / stats.ReloadTime);
        }

        public void PlayReload()
        {
            UpdateSpeedFactors();
            Animator.SetTrigger("Reload");
            ReloadSfx.Play();
        }

        public void FinishReload() => FinishReloadSfx.Play();
    }
}