﻿using System;
using Assets.Scripts.GameCommon;
using Assets.Scripts.Shop.GenerationParameters;
using UnityEngine;

namespace Assets.Scripts.InMission.Weapons
{
    [CreateAssetMenu(fileName = "Stats", menuName = "Weapons/CreateConfig")]
    public class WeaponStats : ScriptableObject
    {
        public int Level;
        public string ModelName;
        public WeaponType WeaponType;
        public bool IsAutomatic;
        public int WeaponHandCount;
        public int Damage;
        public int MagazineSize;
        public float CycleRate;
        public float ReloadTime;
        public float Accuracy;
        public float RecoilStrength;
        public float RecoilRecovery;
        public int CashValue;

        public void AddLevel(GPPerLevelStats perLevelBonuses)
        {
            Level++;
            Damage += (int)Mathf.Clamp(Damage * perLevelBonuses.BonusDamage, 1, int.MaxValue);
            MagazineSize += (int)Mathf.Clamp(MagazineSize * perLevelBonuses.BonusMagazineSize, 1, int.MaxValue);
            Accuracy += Accuracy * perLevelBonuses.BonusAccuracy;
            RecoilRecovery += RecoilRecovery * perLevelBonuses.BonusRecoilRecovery;
            CycleRate -= CycleRate * perLevelBonuses.BonusCycleRate;
            ReloadTime -= ReloadTime * perLevelBonuses.BonusReloadTime;
            RecoilStrength -= RecoilStrength * perLevelBonuses.BonusRecoilStrength;
            CashValue += (int)(CashValue * perLevelBonuses.BonusValue);
        }
    }
}
