﻿using System;
using System.Collections;
using Assets.Scripts.GameCommon;
using UnityEngine;

namespace Assets.Scripts.InMission.Weapons
{
    public class Weapon : MonoBehaviour
    {
        [SerializeField] public string WeaponId;
        [SerializeField] protected WeaponView View;
        [SerializeField] protected Transform FiringPoint;
        [SerializeField] protected WeaponStats BaseStats;

        protected Vector3 FirePosition => FiringPoint.position;
        private bool isCycling;
        private bool isReloading;
        protected int currentAmmo;

        [HideInInspector] public WeaponType WeaponType => BaseStats.WeaponType;
        public WeaponStats CurrentStats => BaseStats;
        public bool IsAuto => BaseStats.IsAutomatic;

        protected float currentAccuracy;
        protected Action<float> onAccuracyChanged = f => { };
        Action onReloadFinished = () => {};

        protected float Inaccuracy => 1 - currentAccuracy/100;
        public bool IsIdle => !isCycling & !isReloading;
        public bool IsEmpty => currentAmmo <= 0;
        public int CurrentAmmoCount => currentAmmo;

        public void Initialize(WeaponStats overrideStats)
        {
            BaseStats = overrideStats;
            currentAmmo = CurrentStats.MagazineSize;
            View.Initialize(CurrentStats);
            currentAccuracy = BaseStats.Accuracy;
        }

        public void Initialize()
        {
            currentAmmo = CurrentStats.MagazineSize;
            View.Initialize(CurrentStats);
            currentAccuracy = BaseStats.Accuracy;
        }

        public void Initialize(Action<float> onAccuracyChanged, Action onReloadFinished)
        {
            currentAmmo = CurrentStats.MagazineSize;
            View.Initialize(CurrentStats);
            currentAccuracy = BaseStats.Accuracy;
            this.onAccuracyChanged = onAccuracyChanged;
            this.onReloadFinished = onReloadFinished;
        }

        void FixedUpdate()
        {
            if (IsIdle)
            {
                currentAccuracy = Mathf.Clamp(currentAccuracy + (BaseStats.RecoilRecovery * Time.fixedDeltaTime), 1,
                    BaseStats.Accuracy);
                onAccuracyChanged(Inaccuracy);
            }
        }

        public virtual void Fire() { }

        public void Reload()
        {
            if (IsIdle)
            {
                View.PlayReload();
                StartCoroutine(WaitForReload());
            }
        }

        private IEnumerator WaitForReload()
        {
            isReloading = true;
            yield return new WaitForSeconds(CurrentStats.ReloadTime);
            FinishReload();
        }

        void FinishReload()
        {
            isReloading = false;
            currentAmmo = CurrentStats.MagazineSize;
            View.FinishReload();
            onReloadFinished();
        }

        protected void CycleRound() => StartCoroutine(WaitForCycle());

        private IEnumerator WaitForCycle()
        {
            isCycling = true;
            yield return new WaitForSeconds(CurrentStats.CycleRate);
            isCycling = false;
        }
        
        public void PlaceAt(Transform placement)
        {
            transform.SetParent(placement);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
    }
}
