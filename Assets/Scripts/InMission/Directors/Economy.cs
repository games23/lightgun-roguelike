﻿using System;
using Assets.Scripts.InMission.Actor;
using Assets.Scripts.Persistence.Session;

namespace Assets.Scripts.InMission.Directors
{
    public class Economy
    {
        int Cash => SessionProgression.Funds;
        Action<int> onUpdateCash;

        public Economy(Action<int> onUpdateCash)
        {
            this.onUpdateCash = onUpdateCash;
            onUpdateCash(Cash);
        }

        public void AddAmount(int amount)
        {
            SessionProgression.AddFunds(amount);
            onUpdateCash(Cash);
        }

        public void SpendAmount(int amount)
        {
            SessionProgression.SubtractFunds(amount);
            onUpdateCash(Cash);
        }
        
        public bool CanSpendAmount(int cost) => Cash >= cost;
    }
}