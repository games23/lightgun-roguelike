﻿using System;
using System.Collections;
using System.Linq;
using Assets.Scripts.InMission.Navigation;
using Assets.Scripts.InMission.Player;
using Assets.Scripts.InMission.UI;
using Assets.Scripts.Persistence.Session;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.InMission.Directors
{
    public class LevelDirector : MonoBehaviour
    {
        [Header("References")] [SerializeField]
        PlayerInitializer playerInitializer;
        [SerializeField] EnemyDirector enemyDirector;
        [SerializeField] CoverSpotProvider CoverSpots;
        [SerializeField] NavPointProvider NavPoints;
        [SerializeField] SpawnPointProvider SpawnPoints;
        [SerializeField] WaveDirector waveDirector;
        [SerializeField] Economy economy;
        [SerializeField] HUD hud;
        [SerializeField] MissionStatusView missionStatusView;

        [SerializeField] float levelEndDelay;
        [SerializeField] LevelWavesConfiguration[] allLevels;
        
        void Start()
        {
            InitializeAll();
            missionStatusView.ShowStartRound(SessionProgression.Level.ToString());
        }

        void InitializeAll()
        {
            SessionProgression.AdvanceLevel();
            economy = new Economy(hud.UpdateCash);

            playerInitializer.Initialize(OnGameOver);
            SpawnPoints.Initialize();
            NavPoints.Initialize();
            CoverSpots.Initialize();
            enemyDirector.Initialize(SpawnPoints, CoverSpots, economy.AddAmount, NoActorsAlive);
            waveDirector.Initialize(
                allLevels.First(l => l.ChallengeLevel == SessionProgression.Level), 
                enemyDirector, 
                hud.UpdateWaves);
        }

        void OnGameOver()
        {
            StartCoroutine(WaitAndDo(levelEndDelay / 2, missionStatusView.ShowGameOver));
            StartCoroutine(WaitAndDo(levelEndDelay, () => SceneManager.LoadScene("Menu")));
        }

        void NoActorsAlive()
        {
            if (waveDirector.LastWaveTriggered)
            {
                if (SessionProgression.Level >= allLevels.Length)
                {
                    StartCoroutine(WaitAndDo(levelEndDelay / 2, missionStatusView.ShowGameWin));
                    StartCoroutine(WaitAndDo(levelEndDelay, () => SceneManager.LoadScene("Menu")));
                }
                else
                {
                    StartCoroutine(WaitAndDo(levelEndDelay/2, missionStatusView.ShowRoundWin));
                    StartCoroutine(WaitAndDo(levelEndDelay, () => SceneManager.LoadScene("Shop")));
                }
            }
        }

        private IEnumerator WaitAndDo(float time, Action doOnWait)
        {
            yield return new WaitForSeconds(time);
            doOnWait();
        }
    }
}
