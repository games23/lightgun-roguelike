﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.InMission.Directors
{
    public class WaveDirector : MonoBehaviour
    {
        EnemyDirector enemyDirector;
        int currentWave;

        int enemiesToSpawn;
        public bool LastWaveTriggered => currentWave >= configuration.MaxWaves;

        Action<int, int> onNewWave;
        LevelWavesConfiguration configuration;

        public void Initialize(LevelWavesConfiguration configuration, EnemyDirector enemyDirector, Action<int, int> onNewWave)
        {
            this.configuration = configuration;
            this.onNewWave = onNewWave;
            this.enemyDirector = enemyDirector;
            enemiesToSpawn = configuration.FirstWaveAmount;
            SpawnNextWave();
        }

        IEnumerator WaitForNewWave()
        {
            yield return new WaitForSeconds(Random.Range(configuration.MinWaitTime, configuration.MaxWaitTime));
            SpawnNextWave();
        }

        void SpawnNextWave()
        {
            currentWave++;
            foreach(var _ in Enumerable.Range(0, enemiesToSpawn))
                enemyDirector.SpawnActor(configuration.GetEnemyLevel());

            enemiesToSpawn += Mathf.Clamp((int)(enemiesToSpawn * configuration.IncrementPerWave), 1, int.MaxValue);

            if(currentWave < configuration.MaxWaves)
                StartCoroutine(WaitForNewWave());

            onNewWave(currentWave, configuration.MaxWaves);
        }
    }
}
