﻿using System;
using System.Linq;
using Assets.Scripts.GameCommon;
using UnityEngine;

namespace Assets.Scripts.InMission.Directors
{
    [CreateAssetMenu(fileName = "Level", menuName = "Configuration/Level")]
    public class LevelWavesConfiguration : ScriptableObject
    {
        public int ChallengeLevel;
        public float MinWaitTime;
        public float MaxWaitTime;
        public float IncrementPerWave;
        public int FirstWaveAmount;
        public int MaxWaves;
        public ChallengeWeightPair[] enemyLevels;

        WeightedList<int> weightedLevels;

        public int GetEnemyLevel()
        {
            if(weightedLevels == null)
                weightedLevels = new WeightedList<int>(enemyLevels.Select(el => new WeightedListItem<int>(el.ChallengeLevel, el.Weight)).ToArray());

            return weightedLevels.PickOne();
        }
    }

    [Serializable]
    public struct ChallengeWeightPair
    {
        public int ChallengeLevel;
        public int Weight;
    }
}