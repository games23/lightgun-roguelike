﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon;
using Assets.Scripts.InMission.Actor;
using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Navigation;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Directors
{
    public class EnemyDirector : MonoBehaviour
    {
        [SerializeField] EnemyController[] actorPrefabs;
        [SerializeField] Weapon[] Weapons;
        [SerializeField] Health playerHealth;
        [SerializeField] Humanoid playerBody;

        IList<EnemyController> actors = new List<EnemyController>();
        SpawnPointProvider spawnPoints;
        CoverSpotProvider coverProvider;
        Action onNoActorsAlive;
        Action<int> onActorKilled;

        private Weapon PickWeapon => Weapons.PickOne();
        
        public void Initialize(
            SpawnPointProvider spawnPoints,
            CoverSpotProvider coverProvider,
            Action<int> onActorKilled,
            Action onNoActorsAlive)
        {
            this.onNoActorsAlive = onNoActorsAlive;
            this.spawnPoints = spawnPoints;
            this.coverProvider = coverProvider;
            this.onActorKilled = onActorKilled;
        }

        public void SpawnActor(int level)
        {
            var candidate = spawnPoints.GetAny();
            if (candidate.Exists)
            {
                var actor = Instantiate(actorPrefabs.First(a => a.Level == level));
                actors.Add(actor);
                actor.transform.SetParent(transform);
                actor.Initialize(coverProvider, playerBody, playerHealth, candidate.Value.Position,
                    OnActorDie, PickWeapon);
            }
        }

        void OnActorDie(EnemyController corpse)
        {
            onActorKilled(corpse.Bounty);
            if (!actors.Any(a => a.IsAlive))
                onNoActorsAlive();
        }

        public IEnumerable<EnemyBrain> GetActorsInRange(Vector3 source, float range) =>
            actors.Select(c => new {brain = c.Brain, distance = Vector3.Distance(c.transform.position, source)})
                .Where(cd => cd.distance <= range)
                .Select(cd => cd.brain)
                .ToArray();
    }
}
