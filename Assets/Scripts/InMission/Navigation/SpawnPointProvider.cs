﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon;
using Assets.Scripts.InMission.Common;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.InMission.Navigation
{
    public class SpawnPointProvider : MonoBehaviour
    {
        private IList<SpawnPoint> spawnPoints;
        
        public void Initialize() => 
            spawnPoints = GetComponentsInChildren<SpawnPoint>().ToList();

        public Maybe<SpawnPoint> GetAny() => new Maybe<SpawnPoint>(spawnPoints.PickOne());

        public Maybe<SpawnPoint> GetSpawnPoint()
        {
            if (!spawnPoints.Any())
                return Maybe<SpawnPoint>.Empty();

            var candidate = spawnPoints.PickOne();
            spawnPoints.Remove(candidate);
            return new Maybe<SpawnPoint>(candidate);
        }
    }
}
