﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon;
using Assets.Scripts.InMission.Common;
using UnityEngine;

namespace Assets.Scripts.InMission.Navigation
{
    public class NavPointProvider : MonoBehaviour
    {
        private IEnumerable<NavPoint> navPoints;
        private IEnumerable<EvacuationPoint> evacs;

        public void Initialize()
        {
            navPoints = GetComponentsInChildren<NavPoint>();
            evacs = GetComponentsInChildren<EvacuationPoint>();
        }

        public Vector3 GetRandomNavPoint() => navPoints.PickOne().Position;

        public EvacuationPoint GetClosestEvacPoint(Vector3 source) => evacs
            .Select(c => new { spot = c, distance = Vector3.Distance(c.Position, source) })
            .OrderBy(csd => csd.distance)
            .First().spot;
    }
}
