﻿using UnityEngine;

namespace Assets.Scripts.InMission.Navigation
{
    public class CoverSpot : MonoBehaviour
    {
        public LayerMask HitLayerMask;
        public Vector3 Position => transform.position;

        public bool IsCoveredFrom(Vector3 target)
        {
            var directionToTarget = (target - transform.position).normalized;
            var ray = new Ray(transform.position, directionToTarget);
            RaycastHit hit;

            return Physics.Raycast(ray, out hit, 1f, HitLayerMask);
        }
    }
}