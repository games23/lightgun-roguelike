﻿using Assets.Scripts.GameCommon;
using UnityEngine;

namespace Assets.Scripts.InMission.Player
{
    public class PlayerAim : MonoBehaviour
    {
        [SerializeField] Vector3 centralFocusPoint;
        [SerializeField] float maxHeadTurn;

        Camera camera;
        Transform weaponHolder;
        Transform characterHead;

        public void Initialize(Camera camera, Transform weaponHolder, Transform characterHead)
        {
            this.camera = camera;
            this.weaponHolder = weaponHolder;
            this.characterHead = characterHead;
        }

        public void AimAt(Vector2 viewPortPoint)
        {
            AimWeapon(viewPortPoint);
            TurnHead(viewPortPoint);
        }

        void AimWeapon(Vector2 viewPortPoint)
        {
            weaponHolder.LookAt(AimPoint(viewPortPoint));
            weaponHolder.NormalizeRoll();
        }

        void TurnHead(Vector2 viewPortPoint)
        {
            characterHead.LookAt(Vector3.Lerp(centralFocusPoint, AimPointToVoid(viewPortPoint), maxHeadTurn));
            characterHead.NormalizeRoll();
        }

        Vector3 AimPoint(Vector2 viewPortPoint)
        {
            var ray = camera.ScreenPointToRay(viewPortPoint);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 300f))
            {
                return hit.point;
            }

            return AimPointToVoid(viewPortPoint);
        }

        Vector3 AimPointToVoid(Vector2 viewPortPoint)
        {
            var ray = camera.ScreenPointToRay(viewPortPoint);
            return ray.GetPoint(200);
        }
    }
}
