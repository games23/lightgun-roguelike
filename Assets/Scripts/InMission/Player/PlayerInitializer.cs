﻿using System;
using System.Linq;
using Assets.Scripts.GameCommon.Services;
using Assets.Scripts.InMission.Actor;
using Assets.Scripts.InMission.Character;
using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.UI;
using Assets.Scripts.Persistence.Session;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.InMission.Player
{
    public class PlayerInitializer : MonoBehaviour
    {
        [Header("Intenal")]
        [SerializeField] Camera camera;
        [SerializeField] Transform head;
        [SerializeField] Transform hand;
        [SerializeField] Transform characterHead;
        [SerializeField] WeaponController weaponController;
        [SerializeField] PlayerInput playerInput;
        [SerializeField] PlayerAim playerAim;
        [SerializeField] Health health;
        [SerializeField] VoiceSfx voice;

        [Header("External")]
        [SerializeField] Text bulletCounter;
        [SerializeField] DynamicCrosshair crosshairUI;
        [SerializeField] Animation hurtUI;
        [SerializeField] UnitCounter healthCounter;
        [SerializeField] WeaponService weaponService;
        [SerializeField] CameraShake cameraShake;
        Action onPlayerLose;

        public void Initialize(Action onPlayerLose)
        {
            this.onPlayerLose = onPlayerLose;
            SpawnWeapons();
            playerAim.Initialize(camera, head, characterHead);
            weaponController.Initialize(UpdateBulletCount, crosshairUI.ScaleToAccuracy);
            playerInput.Initialize(playerAim, weaponController);
            health.Initialize(OnGetHit, OnDeath);
            healthCounter.Initialize(health.InitialHp);
        }

        void SpawnWeapons()
        {
            if (CharacterLoadout.Weapons().Any())
            {
                foreach (var w in CharacterLoadout.Weapons())
                    weaponService.SpawnAt(hand, w);
            }
            else
            {
                var defaultGun = weaponService.SpawnDefaultAt(hand);
                CharacterLoadout.SaveWeapons(defaultGun.CurrentStats);
            }
        }

        void OnGetHit()
        {
            voice.PlayHurt();
            healthCounter.Spend();
            hurtUI.Play();
            cameraShake.Shake();
        }

        void OnDeath()
        {
            voice.PlayDie();
            playerInput.enabled = false;
            onPlayerLose();
        }

        void UpdateBulletCount(int count) => bulletCounter.text = count.ToString();
    }
}
