using Assets.Scripts.InMission.Character;
using Assets.Scripts.InMission.Player;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    WeaponController weaponController;
    PlayerAim aim;

    public void Initialize(PlayerAim aim, WeaponController weaponController)
    {
        this.weaponController = weaponController;
        this.aim = aim;
    }

    void Update()
    {
        aim.AimAt(Input.mousePosition);

        if (Input.GetButton("Fire"))
            weaponController.FireAuto();

        if (Input.GetButtonDown("Fire"))
            weaponController.FireSemi();

        if (Input.GetButtonDown("Reload"))
            weaponController.Reload();

        if (Input.GetKeyDown(KeyCode.Tab))
            weaponController.SwitchWeapon();
    }


}
