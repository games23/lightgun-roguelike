﻿using System;
using System.Linq;
using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Navigation;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class EnemyController : MonoBehaviour
    {
        public string ObjectId { get; private set; }
        public int Bounty => bounty;

        [SerializeField] Health health;
        [SerializeField] Ragdoll ragdoll;
        [SerializeField] Animator animator;
        [SerializeField] ActorAnimator actorAnimator;
        [SerializeField] Navigator navigator;
        [SerializeField] EnemyBrain brain;
        [SerializeField] ActorWeapon weapon;
        [SerializeField] ActorAiming aiming;
        [SerializeField] Humanoid humanoidSelf;
        [SerializeField] Transform viewPoint;
        [SerializeField] VoiceSfx voice;
        [SerializeField] ActorAttack attack;
        [SerializeField] int bounty;
        [SerializeField] int level;

        private Action<EnemyController> onDeath;
        public EnemyBrain Brain => brain;
        public int Level => level;

        public bool IsAlive { get; private set; }

        public void Initialize(CoverSpotProvider coverProvider, Humanoid targetBody, Health targetHealth, Vector3 spawnLocation, Action<EnemyController> onDeath, Weapon WeaponPrefab)
        {
            ObjectId = $"enemy-{Guid.NewGuid().ToString().Substring(0, 5)}";
            IsAlive = true;
            actorAnimator.Initialize(animator);
            weapon.Initialize(actorAnimator, viewPoint, humanoidSelf.WeaponGrab, WeaponPrefab);
            navigator.Initialize(actorAnimator, spawnLocation);
            ragdoll.Initialize();
            health.Initialize(OnHurt, OnDie);
            aiming.Initialize(weapon, actorAnimator, targetBody, humanoidSelf, viewPoint, attack.Attempt, attack.Abort);
            Brain.Initialize(navigator, coverProvider, targetBody, actorAnimator, aiming);
            attack.Initialize(targetHealth.ReceiveDamage);

            this.onDeath = onDeath;
        }

        void OnHurt()
        {
            actorAnimator.GetHit();
            voice.PlayHurt();
        }

        void OnDie()
        {
            ragdoll.TurnOnRagdoll();
            animator.enabled = false;
            navigator.Shutdown();
            Brain.Shutdown();
            IsAlive = false;
            voice.PlayDie();
            attack.Abort();

            onDeath(this);
        }
    }
}
