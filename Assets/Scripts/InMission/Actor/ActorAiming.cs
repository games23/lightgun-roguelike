﻿using System;
using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class ActorAiming : MonoBehaviour
    {
        public float TurnRate;
        public float LockInTolerance;
        public float EngageDistance;
        public LayerMask HitLayer;
        [SerializeField] Transform hip;
        [SerializeField] Vector3 lookOffset;

        private ActorWeapon weapon;
        private ActorAnimator animator;
        private Humanoid target;
        private Humanoid self;
        private Transform viewPoint;

        Quaternion hipDefaultRotation;
        Action onAiming;
        Action onStopAiming;

        bool ReadyToFire => TargetIsInFront && TargetIsClose && TargetVisible;

        private bool TargetIsClose => 
            Vector3.Distance(transform.position, target.CenterOfMass) < EngageDistance;

        private bool TargetIsInFront =>
            Mathf.Abs(transform.InverseTransformPoint(target.CenterOfMass).x) < LockInTolerance;

        public bool TargetVisible
        {
            get
            {
                var directionToTarget = (target.CenterOfMass - self.Head).normalized;
                var ray = new Ray(self.Head, directionToTarget);
                RaycastHit hit;

                return Physics.Raycast(ray, out hit, 200f, HitLayer) &&
                       hit.collider.GetComponent<Humanoid>() &&
                       hit.collider.GetComponent<Humanoid>().Equals(target);
            }
        }

        public void Initialize(ActorWeapon weapon, ActorAnimator animator,
            Humanoid target, Humanoid self, Transform viewPoint,
            Action onAiming, Action onStopAiming)
        {
            hipDefaultRotation = hip.localRotation;
            this.self = self;
            this.weapon = weapon;
            this.animator = animator;
            this.target = target;
            this.viewPoint = viewPoint;
            this.onAiming = onAiming;
            this.onStopAiming = onStopAiming;

            enabled = false;
        }

        void Update()
        {
            var deltaAlignment = transform.InverseTransformPoint(target.CenterOfMass).x;
            transform.Rotate(Vector3.up * TurnRate * deltaAlignment);

            if (ReadyToFire)
            {
                hip.LookAt(target.CenterOfMass + lookOffset);
                viewPoint.LookAt(target.CenterOfMass);
                weapon.Fire();
            }
        }

        public void StopAiming()
        {
            animator.RestAim();
            hip.localRotation = hipDefaultRotation;
            enabled = false;
            onStopAiming();
        }

        public void StartAiming()
        {
            onAiming();
            animator.Aim();
            enabled = true;
        }
    }
}
