﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.GameCommon;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.InMission.Actor
{
    public class ActorAttack : MonoBehaviour
    {
        [SerializeField] ParticleSystem indicator;
        [SerializeField] float chance;
        [SerializeField] float minQuickness;
        [SerializeField] float maxQuickness;
        [SerializeField] int damage;

        Action<int> onSuccessfulAttack;

        public void Initialize(Action<int> onSuccessfulAttack) => 
            this.onSuccessfulAttack = onSuccessfulAttack;

        public void Attempt()
        {
            if (RandomHelper.RollD100(chance))
            {
                var quickness = Random.Range(minQuickness, maxQuickness);
                var mainParticle = indicator.main;
                mainParticle.startLifetime = new ParticleSystem.MinMaxCurve(quickness);
                indicator.Play();
                StartCoroutine(WaitForAttack(quickness));
            }
        }

        IEnumerator WaitForAttack(float time)
        {
            yield return new WaitForSeconds(time);
            onSuccessfulAttack(damage);
        }

        public void Abort()
        {
            indicator.Clear();
            StopAllCoroutines();
        }
    }
}
