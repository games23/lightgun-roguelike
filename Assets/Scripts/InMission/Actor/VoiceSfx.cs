﻿using Assets.Scripts.GameCommon;
using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class VoiceSfx : MonoBehaviour
    {
        [SerializeField] AudioSource audioSource;
        [SerializeField] AudioClip[] hurtSfx;
        [SerializeField] AudioClip[] dieSfx;

        public void PlayHurt()
        {
            audioSource.clip = hurtSfx.PickOne();
            audioSource.Play();
        }

        public void PlayDie()
        {
            audioSource.clip = dieSfx.PickOne();
            audioSource.Play();
        }
    }
}
