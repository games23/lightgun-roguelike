﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Character
{
    public class WeaponController : MonoBehaviour
    {
        [SerializeField] Animator handAnimator;

        Weapon currentGun;
        List<Weapon> guns;
        float slowestFiringFactor = 0.4f;
        Action<int> onBulletCountUpdate;

        public void Initialize(Action<int> onBulletCountUpdate, Action<float> onAccuracyChanged)
        {
            this.onBulletCountUpdate = onBulletCountUpdate;

            guns = GetComponentsInChildren<Weapon>().ToList();

            foreach(var g in guns)
                g.Initialize(onAccuracyChanged, OnReloadFinished);

            HideAllGuns();
            currentGun = guns.First();
            currentGun.gameObject.SetActive(true);

            onBulletCountUpdate(currentGun.CurrentStats.MagazineSize);
            handAnimator.SetFloat("FireSpeedFactor", 1f / currentGun.CurrentStats.CycleRate);
            handAnimator.SetFloat("ReloadSpeedFactor", 1f / currentGun.CurrentStats.ReloadTime);
        }

        void OnReloadFinished() => onBulletCountUpdate(currentGun.CurrentAmmoCount);

        void HideAllGuns()
        {
            foreach (var g in guns)
                g.gameObject.SetActive(false);
        }

        public void FireAuto()
        {
            if(currentGun.IsAuto)
                Fire();
        }

        public void FireSemi()
        {
            if (!currentGun.IsAuto)
                Fire();
        }

        void Fire()
        {
            if (currentGun && currentGun.IsIdle)
            {
                if (currentGun.IsEmpty)
                    Reload();
                else
                {
                    currentGun.Fire();
                    onBulletCountUpdate(currentGun.CurrentAmmoCount);
                    handAnimator.SetTrigger("Fire");
                }
            }
        }

        public void Reload()
        {
            currentGun.Reload();
            handAnimator.SetTrigger("Reload");
        }

        public void SwitchWeapon()
        {
            if (currentGun.IsIdle)
            {
                HideAllGuns();
                var index = guns.IndexOf(currentGun);
                index++;
                if (index >= guns.Count)
                    index = 0;

                currentGun = guns[index];
                currentGun.gameObject.SetActive(true);
                onBulletCountUpdate(currentGun.CurrentAmmoCount);
                handAnimator.SetFloat("FireSpeedFactor", 1f / currentGun.CurrentStats.CycleRate);
                handAnimator.SetFloat("ReloadSpeedFactor", 1f / currentGun.CurrentStats.ReloadTime);
            }
        }
    }
}
