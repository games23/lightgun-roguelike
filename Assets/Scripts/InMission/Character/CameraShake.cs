﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.InMission.Character
{
    public class CameraShake : MonoBehaviour
    {
        [SerializeField] float violence;
        [SerializeField] float duration;

        Vector3 initialPosition;

        public void Shake()
        {
            initialPosition = transform.localPosition;
            StartCoroutine(DoShake());
        }

        IEnumerator DoShake()
        {
            float elapsed = 0;
            while (elapsed < duration)
            {
                transform.localPosition = initialPosition + Random.insideUnitSphere * violence;
                elapsed += Time.deltaTime;
                yield return null;
            }

            transform.localPosition = initialPosition;
        }
    }
}
