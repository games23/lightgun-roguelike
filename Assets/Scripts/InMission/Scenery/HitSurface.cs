﻿using UnityEngine;

namespace Assets.Scripts.InMission.Scenery
{
    public class HitSurface : MonoBehaviour
    {
        public GameObject ImpactHolePrefab;

        public void HitHere(Vector3 hitPosition, Vector3 normalDirection)
        {
            var hole = Instantiate(ImpactHolePrefab, hitPosition, Quaternion.identity).transform;
            hole.LookAt(hitPosition - normalDirection);
            hole.SetParent(transform);
        }
    }
}
