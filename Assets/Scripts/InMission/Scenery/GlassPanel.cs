﻿using Assets.Scripts.InMission.Common;
using UnityEngine;

namespace Assets.Scripts.InMission.Scenery
{
    public class GlassPanel : MonoBehaviour
    {
        [SerializeField] Health health;
        [SerializeField] GameObject staticPanel;
        [SerializeField] GameObject shatteredPanel;
        [SerializeField] AudioSource shatterSfx;
        
        void Start()
        {
            health.Initialize(() => { }, Destroy);
            foreach(var p in GetComponentsInChildren<Push>())
                p.Initialize();
        }

        void Destroy()
        {
            staticPanel.SetActive(false);
            shatterSfx.Play();
            shatteredPanel.SetActive(true);
        }
    }
}
