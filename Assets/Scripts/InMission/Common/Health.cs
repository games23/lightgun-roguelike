﻿using System;
using UnityEngine;

namespace Assets.Scripts.InMission.Common
{
    public class Health : MonoBehaviour
    {
        public int InitialHp;
        private int currentHp;

        private Action onDeath;
        private Action onGetHit;
        private Action<int> onGetHitWithArgs;

        public void Initialize(Action onGetHit, Action onDeath)
        {
            this.onDeath = onDeath;
            this.onGetHit = onGetHit;
            this.onGetHitWithArgs = (a) => { };
            Initialize();
        }

        public void Initialize(Action<int> onGetHit, Action onDeath)
        {
            this.onDeath = onDeath;
            this.onGetHitWithArgs = onGetHit;
            this.onGetHit = () => { };
            Initialize();
        }

        void Initialize()
        {
            currentHp = InitialHp;
            foreach (var hbp in GetComponentsInChildren<HitBodyPart>())
                hbp.Initialize(this);
        }

        public void ReceiveDamage(int amount)
        {
            if (currentHp > 0)
            {
                currentHp -= amount;
                onGetHit();
                onGetHitWithArgs(currentHp);
                if (currentHp <= 0)
                    onDeath();
            }
        }
    }
}
