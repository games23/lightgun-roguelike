﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.InMission.Common
{
    public class AutoDispose : MonoBehaviour
    {
        public float Lifetime;

        void Start()
        {
            StartCoroutine(WaitAndKill());
        }

        private IEnumerator WaitAndKill()
        {
            yield return new WaitForSeconds(Lifetime);
            Destroy(gameObject);
        }
    }
}
