﻿namespace Assets.Scripts.InMission.Common
{
    public interface Damageable
    {
        void GetHit(int damage);
    }
}
