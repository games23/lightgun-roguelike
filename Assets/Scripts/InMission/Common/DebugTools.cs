﻿using UnityEngine;

namespace Assets.Scripts.InMission.Common
{
    public class DebugTools : MonoBehaviour
    {
        void Update()
        {
            Debug.DrawLine(transform.position, transform.position + Vector3.forward, Color.blue);
            Debug.DrawLine(transform.position, transform.position + Vector3.right, Color.red);

            Debug.DrawLine(transform.position, transform.position + transform.forward, Color.magenta);
            Debug.DrawLine(transform.position, transform.position + transform.right, Color.yellow);
        }
    }
}
