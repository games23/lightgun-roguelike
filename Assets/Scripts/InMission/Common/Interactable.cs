﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.InMission.Common
{
    public class Interactable : MonoBehaviour
    {
        private Action<Transform> onInteractedWith;

        public void Initialize(Action<Transform> onInteractedWith) => this.onInteractedWith = onInteractedWith;

        public void OnInteract(Transform interacter) => onInteractedWith(interacter);
    }
}
