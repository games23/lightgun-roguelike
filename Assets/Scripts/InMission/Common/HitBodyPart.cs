﻿using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Common
{
    public class HitBodyPart : MonoBehaviour, Damageable
    {
        public float DamageMultiplier;

        protected Health health;

        public void Initialize(Health health) => this.health = health;

        public virtual void GetHit(int damage) => 
            health.ReceiveDamage(Mathf.CeilToInt(damage * DamageMultiplier));
    }
}
