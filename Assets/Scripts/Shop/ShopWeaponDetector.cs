﻿using System;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Shop.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Shop
{
    public class ShopWeaponDetector : MonoBehaviour
    {
        Weapon weapon;
        WeaponTooltip tooltip;
        Action<Weapon> onClick;

        public void Initialize(WeaponTooltip tooltip, Weapon weapon, Action<Weapon> onClick)
        {
            this.tooltip = tooltip;
            this.weapon = weapon;
            this.onClick = onClick;
        }

        public void OnMouseOver() => tooltip.Show(weapon.CurrentStats);

        public void OnMouseExit() => tooltip.Hide();

        void OnMouseDown()
        {
            Debug.Log("on click");
            onClick(weapon);
        }
    }
}
