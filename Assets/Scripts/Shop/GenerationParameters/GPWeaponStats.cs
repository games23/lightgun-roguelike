﻿using UnityEngine;
using Assets.Scripts.GameCommon;

namespace Assets.Scripts.Shop.GenerationParameters
{
    [CreateAssetMenu(fileName = "Stats", menuName = "Generation Parameters/For Weapon")]
    public class GPWeaponStats : ScriptableObject
    {
        public WeaponType WeaponType;

        public int MinCashValue;
        public int MaxCashValue;

        public int MinDamage;
        public int MaxDamage;

        public int MinMagazineSize;
        public int MaxMagazineSize;

        public float MinCycleRate;
        public float MaxCycleRate;

        public float MinReloadTime;
        public float MaxReloadTime;

        public float MinAccuracy;
        public float MaxAccuracy;

        public float MinRecoilStrength;
        public float MaxRecoilStrength;

        public float MinRecoilRecovery;
        public float MaxRecoilRecovery;

        public bool IsAutomatic;
        public int WeaponHandCount;
    }
}
