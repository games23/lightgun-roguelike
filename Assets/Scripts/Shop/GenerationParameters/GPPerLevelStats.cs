﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.GameCommon;
using UnityEngine;

namespace Assets.Scripts.Shop.GenerationParameters
{
    [CreateAssetMenu(fileName = "Stats", menuName = "Generation Parameters/For Weapon per level")]
    public class GPPerLevelStats : ScriptableObject
    {
        public WeaponType WeaponType;

        public float BonusDamage;
        public float BonusMagazineSize;
        public float BonusCycleRate;
        public float BonusReloadTime;
        public float BonusAccuracy;
        public float BonusRecoilStrength;
        public float BonusRecoilRecovery;
        public float BonusValue;
    }
}
