﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon.Services;
using Assets.Scripts.InMission.Directors;
using Assets.Scripts.InMission.UI;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Persistence.Session;
using Assets.Scripts.Shop.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Shop
{
    public class ArmoryDirector : MonoBehaviour
    {
        [SerializeField] Transform weaponSpot;
        [SerializeField] InventoryPanel inventory;
        [SerializeField] int inventorySize;
        [SerializeField] float weaponSellRatio;
        [SerializeField] float weaponUpgradeRatio;
        [SerializeField] CashCounter cashCounter;
        [SerializeField] Text sellButtontext;
        [SerializeField] Text upgradeButtontext;
        [SerializeField] WeaponTooltip weaponPanel;

        UISfxPlayer sfx;
        Weapon currentWeapon;
        List<WeaponStats> inventoryWeapons;
        WeaponService weaponService;
        Economy economy;

        public void Initialize(WeaponService weaponService, UISfxPlayer sfx)
        {
            this.sfx = sfx;
            economy = new Economy(cashCounter.UpdateCash);
            this.weaponService = weaponService;
            inventoryWeapons = CharacterLoadout.Weapons().ToList();
            inventory.Initialize(inventorySize, SelectWeapon);

            foreach (var w in inventoryWeapons)
                inventory.AddWeaponSlot(w);

            SelectDefaultWeapon();
        }

        void SelectDefaultWeapon()
        {
            if (inventoryWeapons.Any())
                SelectWeapon(inventoryWeapons.First());
            else
                weaponPanel.Hide();

        }

        void SelectWeapon(WeaponStats weapon)
        {
            if(currentWeapon)
                Destroy(currentWeapon.gameObject);

            currentWeapon = weaponService.CreateAt(weapon.WeaponType, weaponSpot);
            currentWeapon.Initialize(weapon);
            sellButtontext.text = $"SELL: ${(int)(weapon.CashValue * weaponSellRatio)}";
            upgradeButtontext.text = $"UPGRADE: ${(int)(weapon.CashValue * weaponUpgradeRatio)}";
            weaponPanel.Show(weapon);
            sfx.PlayClick();
        }

        public void PurchaseWeapon(WeaponStats weapon)
        {
            inventoryWeapons.Add(weapon);
            inventory.AddWeaponSlot(weapon);
            CharacterLoadout.SaveWeapons(inventoryWeapons);
        }

        public void SellWeapon()
        {
            var sellValue = (int) (currentWeapon.CurrentStats.CashValue * weaponSellRatio);
            if (inventoryWeapons.Count > 1)
            {
                economy.AddAmount(sellValue);
                inventoryWeapons.Remove(currentWeapon.CurrentStats);
                inventory.RemoveWeaponSlot(currentWeapon.CurrentStats);
                SelectDefaultWeapon();
                CharacterLoadout.SaveWeapons(inventoryWeapons);
                sfx.PlayCash();
            }
        }

        public void UpgradeWeapon()
        {
            var upgradeCost = (int)(currentWeapon.CurrentStats.CashValue * weaponUpgradeRatio);
            if (economy.CanSpendAmount(upgradeCost))
            {
                economy.SpendAmount(upgradeCost);
                currentWeapon.CurrentStats.AddLevel(weaponService.GetPerLevelStats(currentWeapon.WeaponType));
                SelectWeapon(currentWeapon.CurrentStats);
                CharacterLoadout.SaveWeapons(inventoryWeapons);
                sfx.PlayUpgrade();
            }
            else
                sfx.PlayError();
        }

        public bool CanPurchaseWeapon(int cost) => 
            economy.CanSpendAmount(cost) && inventoryWeapons.Count < inventorySize;
    }
}
