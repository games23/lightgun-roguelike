﻿using System;
using Assets.Scripts.GameCommon.Services;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Shop.UI;
using UnityEngine;

namespace Assets.Scripts.Shop
{
    public class WeaponCrate : MonoBehaviour
    {
        
        [SerializeField] Transform weaponSpot;
        [SerializeField] ShopWeaponDetector detectorPrefab;
        [SerializeField] Animator animator;
        [Header("Vfx")]
        [SerializeField] Gradient rarityGradient;
        [SerializeField] ParticleSystem[] vfxs;

        Weapon activeWeapon;
        WeaponTooltip tooltip;
        Action<Weapon> onWeaponPurchase;
        WeaponService weaponService;

        public void Initialize(WeaponService weaponService, WeaponTooltip tooltip, Action<Weapon> onWeaponPurchase)
        {
            this.weaponService = weaponService;
            this.onWeaponPurchase = onWeaponPurchase;
            this.tooltip = tooltip;
        }

        public void Open()
        {
            animator.SetTrigger("open");
            foreach (var v in vfxs)
                v.Play();
        }

        public void Place(WeaponStats stats)
        {
            if (activeWeapon)
                Destroy(activeWeapon.gameObject);

            activeWeapon = weaponService.CreateAt(stats.WeaponType, weaponSpot);
            activeWeapon.Initialize(stats);
            var detector = Instantiate(detectorPrefab, activeWeapon.transform);
            detector.Initialize(tooltip, activeWeapon, onWeaponPurchase);
            SetUpVfx(stats.Level);
        }

        void SetUpVfx(int level)
        {
            foreach (var v in vfxs)
            {
                var part = v.main;
                part.startColor = rarityGradient.Evaluate((float)level/5);
            }
        }

        public void Close()
        {
            animator.SetTrigger("close");
            foreach (var v in vfxs)
                v.Stop();
        }
    }
}
