﻿using Assets.Scripts.Shop.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Shop
{
    public class ShopNavigation : MonoBehaviour
    {
        [SerializeField] Animator cameraCrane;
        [SerializeField] GameObject armoryUI;
        [SerializeField] GameObject shopUI;
        [SerializeField] UISfxPlayer sfx;

        bool inArmory = true;

        public void SwitchView()
        {
            inArmory = !inArmory;
            cameraCrane.SetTrigger("ChangeView");

            armoryUI.SetActive(inArmory);
            shopUI.SetActive(!inArmory);
            sfx.PlayClick();
        }

        public void GoToMission()
        {
            SceneManager.LoadScene("Mission");
        }
    }
}
