﻿using Assets.Scripts.GameCommon.Services;
using Assets.Scripts.Shop.UI;
using UnityEngine;

namespace Assets.Scripts.Shop
{
    public class DirectorInitializer : MonoBehaviour
    {
        [SerializeField] ShopDirector shop;
        [SerializeField] ArmoryDirector armory;
        [SerializeField] WeaponService weaponService;
        [SerializeField] UISfxPlayer sfx;

        void Start()
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
            armory.Initialize(weaponService, sfx);
            shop.Initialize(weaponService, sfx, armory.PurchaseWeapon, armory.CanPurchaseWeapon);
        }
    }
}
