﻿using System;
using Assets.Scripts.GameCommon.Services;
using Assets.Scripts.InMission.Directors;
using Assets.Scripts.InMission.UI;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Shop.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Shop
{
    public class ShopDirector : MonoBehaviour
    {
        [SerializeField] WeaponTooltip tooltip;
        [SerializeField] WeaponCrate[] crates;
        [SerializeField] CashCounter cashCounter;
        [SerializeField] Text rerollButtonText;
        [SerializeField] int rerollInitialCost;
        [SerializeField] int rerollExtraCost;

        WeaponService weaponService;
        Action<WeaponStats> onWeaponPurchase;
        Func<int, bool> canPurchaseWeapon;
        Economy economy;
        int rerollCost;
        UISfxPlayer sfx;

        public void Initialize(WeaponService weaponService, UISfxPlayer sfx, Action<WeaponStats> onWeaponPurchase, Func<int, bool> canPurchaseWeapon)
        {
            this.sfx = sfx;
            rerollCost = rerollInitialCost;
            UpdateRerollButton();
            economy = new Economy(cashCounter.UpdateCash);
            this.canPurchaseWeapon = canPurchaseWeapon;
            this.weaponService = weaponService;
            this.onWeaponPurchase = onWeaponPurchase;
            InitializeAll();
            OpenAll();
        }

        void UpdateRerollButton() => 
            rerollButtonText.text = $"REROLL ${rerollCost}";

        void InitializeAll()
        {
            foreach (var crate in crates)
                crate.Initialize(weaponService, tooltip, OnWeaponTryPurchase);
        }

        void OnWeaponTryPurchase(Weapon weapon)
        {
            if (canPurchaseWeapon(weapon.CurrentStats.CashValue))
            {
                tooltip.Hide();
                economy.SpendAmount(weapon.CurrentStats.CashValue);
                onWeaponPurchase(weapon.CurrentStats);
                Destroy(weapon.gameObject);
                sfx.PlayBuy();
            }
            else
                sfx.PlayError();
        }

        void OpenAll()
        {
            foreach (var crate in crates)
            {
                var weapon = weaponService.GenerateNew();
                crate.Place(weapon);
                crate.Open();
            }
        }
        
        public void ReRoll()
        {
            if (economy.CanSpendAmount(rerollCost))
            {
                economy.SpendAmount(rerollCost);
                CloseAll();
                Invoke("OpenAll", 2f);
                rerollCost += rerollExtraCost;
                UpdateRerollButton();

                sfx.PlayCash();
            }
            else
                sfx.PlayError();
        }

        void CloseAll()
        {
            foreach (var crate in crates)
                crate.Close();
        }
    }
}
