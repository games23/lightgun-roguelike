﻿using UnityEngine;

namespace Assets.Scripts.Shop.UI
{
    public class UISfxPlayer :MonoBehaviour
    {
        [SerializeField] AudioSource buyWeapon;
        [SerializeField] AudioSource upgradeWeapon;
        [SerializeField] AudioSource clickButton;
        [SerializeField] AudioSource error;
        [SerializeField] AudioSource cash;

        public void PlayBuy() => buyWeapon.Play();
        public void PlayUpgrade() => upgradeWeapon.Play();
        public void PlayClick() => clickButton.Play();
        public void PlayError() => error.Play();
        public void PlayCash() => cash.Play();
    }
}
