﻿using System;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Shop.UI
{
    internal class WeaponSlotButton : MonoBehaviour
    {
        public WeaponStats AttachedWeapon => weapon;

        [SerializeField] Button button;
        [SerializeField] Image icon;
        WeaponStats weapon;
        Action<WeaponStats> onClick;

        public void Initialize(WeaponStats weapon, Sprite iconSprite, Action<WeaponStats> onClick)
        {
            this.weapon = weapon;
            this.onClick = onClick;
            icon.sprite = iconSprite;
            button.onClick.AddListener(SelectWeapon);
        }

        void SelectWeapon() => onClick(weapon);
    }
}