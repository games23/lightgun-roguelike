﻿using System;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Shop.UI
{
    public class WeaponTooltip : MonoBehaviour
    {
        [SerializeField] Vector3 projectionOffset;
        [SerializeField] Text weaponName;
        [SerializeField] Text cost;
        [SerializeField] Text level;
        [SerializeField] Text damage;
        [SerializeField] Text accuracy;
        [SerializeField] Text magSize;
        [SerializeField] Text recoil;
        [SerializeField] Text reloadTime;
        [SerializeField] Text rateOfFire;

        void FillData(WeaponStats stats)
        {
            weaponName.text = stats.ModelName;
            level.text = $"LEVEL: {stats.Level}";
            cost.text = $"COST: ${stats.CashValue}";
            damage.text = $"DAMAGE: {stats.Damage}";
            accuracy.text = $"ACCURACY: {stats.Accuracy:0.00}%";
            magSize.text = $"MAGAZINE SIZE: {stats.MagazineSize}";
            recoil.text = $"RECOIL: {stats.RecoilStrength:0.00}";
            reloadTime.text = $"RELOAD TIME: {stats.ReloadTime:0.00} s";
            rateOfFire.text = $"RATE OF FIRE: {(1f/stats.CycleRate):0.00} b/s";
        }

        public void Show(WeaponStats stats)
        {
            gameObject.SetActive(true);
            FillData(stats);
        }

        public void Hide() => 
            gameObject.SetActive(false);

        void Update() => 
            transform.position = Input.mousePosition + projectionOffset;
    }
}
