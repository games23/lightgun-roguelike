﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.Shop.UI
{
    public class InventoryPanel : MonoBehaviour
    {
        [SerializeField] GameObject emptySlotPrefab;
        [SerializeField] WeaponSlotButton weaponSlotPrefab;
        [SerializeField] Transform slotContainer;
        [SerializeField] WeaponIconPair[] iconCodex;
        Action<WeaponStats> onWeaponSelected;

        List<GameObject> emptySlots;
        List<WeaponSlotButton> occupiedSlots;

        public void Initialize(int inventorySize, Action<WeaponStats> onWeaponSelected)
        {
            emptySlots = new List<GameObject>();
            occupiedSlots = new List<WeaponSlotButton>();
            this.onWeaponSelected = onWeaponSelected;

            foreach (var _ in Enumerable.Range(0, inventorySize))
            {
                AddEmptySlot();
            }
        }

        public void AddWeaponSlot(WeaponStats weapon)
        {
            RemoveEmptySlot();
            var slot = Instantiate(weaponSlotPrefab, slotContainer);
            var icon = iconCodex.First(x => x.WeaponType == weapon.WeaponType).Icon;
            slot.Initialize(weapon, icon, onWeaponSelected);
            occupiedSlots.Add(slot);
        }

        public void RemoveWeaponSlot(WeaponStats weapon)
        {
            var candidate = occupiedSlots.First(os => os.AttachedWeapon == weapon);
            occupiedSlots.Remove(candidate);
            Destroy(candidate.gameObject);
            AddEmptySlot();
        }

        public void AddEmptySlot()
        {
            var emptySlot = Instantiate(emptySlotPrefab, slotContainer);
            emptySlots.Add(emptySlot);
        }
        void RemoveEmptySlot()
        {
            var candidate = emptySlots.Last();
            emptySlots.Remove(candidate);
            Destroy(candidate);
        }
    }

    [Serializable]
    public struct WeaponIconPair
    {
        public WeaponType WeaponType;
        public Sprite Icon;
    }
}