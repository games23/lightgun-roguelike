﻿using Assets.Scripts.Persistence.Session;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.MainMenu
{
    public class MenuNavigation : MonoBehaviour
    {

        void Start()
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }

        public void NewGame()
        {

            CharacterLoadout.Reset();
            SessionProgression.Reset();
            SceneManager.LoadScene("Mission");
        }

        public void Exit() => Application.Quit();
    }
}
