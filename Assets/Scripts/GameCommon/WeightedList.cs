﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameCommon
{
    public class WeightedList<T>
    {
        List<WeightedListItem<T>> rawList;

        List<T> ExtendedList
        {
            get
            {
                var extendedList = new List<T>();
                foreach (var rli in rawList)
                foreach (var _ in Enumerable.Range(0, rli.Weight))
                    extendedList.Add(rli.Value);
                return extendedList;
            }
        }

        public WeightedList(params WeightedListItem<T>[] items) => rawList = items.ToList();

        public T PickOne() => 
            ExtendedList.PickOne();

    }

    public class WeightedListItem<T>
    {
        public readonly T Value;
        public readonly int Weight;

        public WeightedListItem(T value, int weight)
        {
            this.Value = value;
            this.Weight = weight;
        }
    }
}
