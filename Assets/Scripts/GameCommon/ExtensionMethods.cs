﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.GameCommon
{
    public static class ExtensionMethods
    {
        public static T PickOne<T>(this IEnumerable<T> source)
        {
            var randomIndex = Random.Range(0, source.Count());
            return source.ToArray()[randomIndex];
        }

        public static T PickOneExcept<T>(this IEnumerable<T> source, T exception)
        {
            var randomIndex = Random.Range(0, source.Count() - 1);
            return source.Except(exception).ToArray()[randomIndex];
        }

        public static IEnumerable<T> Plus<T>(this IEnumerable<T> source, T itemToAdd) =>
            source.Union(new[] {itemToAdd});

        public static IEnumerable<T> Except<T>(this IEnumerable<T> source, T exception) =>
            source.Except(new[] {exception});

        public static T NextItem<T>(this IList<T> source, T current) => 
            source[source.NextIndex(current)];

        public static int NextIndex<T>(this IList<T> source, T current)
        {
            var nextIndex = source.IndexOf(current) + 1;
            nextIndex = nextIndex >= source.Count ? 0 : nextIndex;
            return nextIndex;
        }

        public static bool MatchId(this string source, string target) =>
            source.ToLower().Equals(target.ToLower());

        public static void NormalizeRoll(this Transform source)
        {
            var euler = source.eulerAngles;
            euler.z = 0;
            source.eulerAngles = euler;
        }
    }
}
