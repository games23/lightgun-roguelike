﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Random = UnityEngine.Random;

namespace Assets.Scripts.GameCommon
{
    public static class RandomHelper
    {
        public static bool RollD100(float chance) => Random.Range(0, 100) < chance;
    }
}
