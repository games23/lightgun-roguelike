﻿namespace Assets.Scripts.GameCommon
{
    public enum WeaponType
    {
        AK,
        Pistol,
        SMG,
        M4,
        MG,
        Rifle
    }
}