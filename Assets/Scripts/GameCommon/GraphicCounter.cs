﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.GameCommon
{
    public class GraphicCounter : MonoBehaviour
    {
        public Image Fill;

        private int maxAmount;

        public void Initialize(int maxAmount, int initialAmount)
        {
            this.maxAmount = maxAmount;
            UpdateCounter(initialAmount);
        }

        public void UpdateCounter(int value) => Fill.fillAmount = (float) value / maxAmount;
        public void ResetCounter() => Fill.fillAmount = 0;
        public void FillCounter() => Fill.fillAmount = 1;

    }
}
