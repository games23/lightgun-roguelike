﻿using System.Linq;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Shop.GenerationParameters;
using UnityEngine;

namespace Assets.Scripts.GameCommon.Services
{
    public class WeaponService : MonoBehaviour
    {
        [SerializeField] Weapon[] weaponPrefabs;
        [SerializeField] GPWeaponStats[] generationParameters;
        [SerializeField] GPPerLevelStats[] perLevelParameters;

        WeaponGenerator Generator {
            get
            {
                if (generator == null)
                    generator = new WeaponGenerator(generationParameters, perLevelParameters);

                return generator;
            }
        }

        WeaponGenerator generator;

        public void SpawnAt(Transform parent, WeaponStats stats)
        {
            var candidate = weaponPrefabs.First(x => x.CurrentStats.WeaponType == stats.WeaponType);
            var weapon = Instantiate(candidate, parent);
            weapon.Initialize(stats);
        }

        public Weapon SpawnDefaultAt(Transform parent)
        {
            var candidate = weaponPrefabs.First(x => x.CurrentStats.WeaponType == WeaponType.Pistol);
            var stats = Generator.Generate(WeaponType.Pistol, 1);
            var weapon = Instantiate(candidate, parent);
            weapon.Initialize(stats);

            return weapon;
        }
        
        public Weapon CreateAt(WeaponType weaponType, Transform where) => 
            Instantiate(weaponPrefabs.First(x => x.CurrentStats.WeaponType == weaponType), @where);

        public WeaponStats GenerateNew() => 
            Generator.Generate();

        public GPPerLevelStats GetPerLevelStats(WeaponType weapontype) =>
            perLevelParameters.First(plp => plp.WeaponType == weapontype);
    }
}
