﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Shop.GenerationParameters;

namespace Assets.Scripts.GameCommon.Services
{
    public class WeaponGenerator
    {
        IEnumerable<GPWeaponStats> generationParameters;
        IEnumerable<GPPerLevelStats> perLevelStatList;

        WeightedList<int> weightedLevels = new WeightedList<int>(
                new WeightedListItem<int>(1, 50),
                new WeightedListItem<int>(2, 25),
                new WeightedListItem<int>(3, 15),
                new WeightedListItem<int>(4, 8),
                new WeightedListItem<int>(5, 2)
            );

        public WeaponGenerator(IEnumerable<GPWeaponStats> generationParameters, IEnumerable<GPPerLevelStats> perLevelStatList)
        {
            this.generationParameters = generationParameters;
            this.perLevelStatList = perLevelStatList;
        }

        public WeaponStats Generate(WeaponType weaponType, int level)
        {
            var parameters = generationParameters.First(gp => gp.WeaponType == weaponType);
            var stats = new WeaponStats
            {
                ModelName = $"{weaponType.ToString()}-{Guid.NewGuid().ToString().Substring(0, 3)}",
                CashValue = UnityEngine.Random.Range(parameters.MinCashValue, parameters.MaxCashValue),
                Level = 1,
                WeaponType = weaponType,
                Accuracy = UnityEngine.Random.Range(parameters.MinAccuracy, parameters.MaxAccuracy),
                CycleRate = UnityEngine.Random.Range(parameters.MinCycleRate, parameters.MaxCycleRate),
                Damage = UnityEngine.Random.Range(parameters.MinDamage, parameters.MaxDamage),
                MagazineSize = UnityEngine.Random.Range(parameters.MinMagazineSize, parameters.MaxMagazineSize),
                ReloadTime = UnityEngine.Random.Range(parameters.MinReloadTime, parameters.MaxReloadTime),
                RecoilRecovery = UnityEngine.Random.Range(parameters.MinRecoilRecovery, parameters.MaxRecoilRecovery),
                RecoilStrength = UnityEngine.Random.Range(parameters.MinRecoilStrength, parameters.MaxRecoilStrength),

                IsAutomatic = parameters.IsAutomatic,
                WeaponHandCount = parameters.WeaponHandCount
            };

            return stats;
        }

        public WeaponStats Generate()
        {
            var rolledType = Enum.GetValues(typeof(WeaponType)).Cast<WeaponType>().PickOne();
            var rolledLevel = weightedLevels.PickOne();

            var parameters = generationParameters.First(gp => gp.WeaponType == rolledType);
            var stats = new WeaponStats
            {
                ModelName = $"{rolledType.ToString()}-{Guid.NewGuid().ToString().Substring(0, 3)}",
                Level = 1,
                CashValue = UnityEngine.Random.Range(parameters.MinCashValue, parameters.MaxCashValue),
                WeaponType = rolledType,
                Accuracy = UnityEngine.Random.Range(parameters.MinAccuracy, parameters.MaxAccuracy),
                CycleRate = UnityEngine.Random.Range(parameters.MinCycleRate, parameters.MaxCycleRate),
                Damage = UnityEngine.Random.Range(parameters.MinDamage, parameters.MaxDamage),
                MagazineSize = UnityEngine.Random.Range(parameters.MinMagazineSize, parameters.MaxMagazineSize),
                ReloadTime = UnityEngine.Random.Range(parameters.MinReloadTime, parameters.MaxReloadTime),
                RecoilRecovery = UnityEngine.Random.Range(parameters.MinRecoilRecovery, parameters.MaxRecoilRecovery),
                RecoilStrength = UnityEngine.Random.Range(parameters.MinRecoilStrength, parameters.MaxRecoilStrength),

                IsAutomatic = parameters.IsAutomatic,
                WeaponHandCount = parameters.WeaponHandCount
            };

            foreach (var _ in Enumerable.Range(0, rolledLevel - 1))
            {
                stats.AddLevel(perLevelStatList.First(x => x.WeaponType == stats.WeaponType));
            }
            
            return stats;
        }
    }
}
