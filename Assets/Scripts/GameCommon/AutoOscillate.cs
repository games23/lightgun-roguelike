﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameCommon
{
    public class AutoOscillate : MonoBehaviour
    {
        [SerializeField] Vector3 vector;
        [SerializeField] float amplitude;
        [SerializeField] float frequency;

        float angle;
        Vector3 initialPosition;

        void Start() => initialPosition = transform.localPosition;

        void Update()
        {
            angle += frequency * Time.deltaTime;
            transform.position += vector * Mathf.Sin(angle) * amplitude * Time.deltaTime;
        }
    }
}
