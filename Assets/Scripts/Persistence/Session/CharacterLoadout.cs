﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InMission.Weapons;

namespace Assets.Scripts.Persistence.Session
{
    public static class CharacterLoadout
    {
        static List<WeaponStats> weapons;

        public static void SaveWeapons(params WeaponStats[] loadout) => 
            weapons = loadout.ToList();

        public static void SaveWeapons(IEnumerable<WeaponStats> loadout) => 
            weapons = loadout.ToList();

        public static IEnumerable<WeaponStats> Weapons()
        {
            if(weapons == null)
                weapons = new List<WeaponStats>();

            return weapons;
        }
        
        public static void Reset() => 
            weapons = new List<WeaponStats>();
    }
}
