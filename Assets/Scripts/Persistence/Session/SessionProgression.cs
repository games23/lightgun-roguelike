﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Persistence.Session
{
    public static class SessionProgression
    {
        public static int Funds { get; private set; }
        public static int Level { get; private set; }

        public static void AddFunds(int value) => Funds += value;
        public static void AdvanceLevel() => Level++;

        public static void SubtractFunds(int value) => Funds -= value;

        public static void Reset()
        {
            Funds = 0;
            Level = 0;
        }
    }
}
