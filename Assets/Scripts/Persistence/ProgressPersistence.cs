﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Persistence
{
    public static class ProgressPersistence
    {
        public static int CurrentProgress { get; private set; }
        
        public static void ModifyProgress(int deltaProgress)
        {
            CurrentProgress += deltaProgress;
            if (CurrentProgress < 0)
                ResetProgress();
        }

        public static void ResetProgress() => CurrentProgress = 0;
    }
}
